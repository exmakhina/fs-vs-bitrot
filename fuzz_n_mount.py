#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Fuzz a filesystem, try to mount it, and compare checksums

import sys, os, subprocess, logging, argparse
import itertools, hashlib

logger = logging.getLogger()


def command_line(cmd: str):
	s = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	if s.returncode != 0:
		m = f"Failed to run {cmd}, ret={s.returncode}"
		raise subprocess.SubprocessError(m)
	logger.debug(f"CMD success: {cmd}")
	return s.stdout


def mount_checksum(file, mountpoint):
	# Try to mount
	try:
		command_line(f"sudo mount -o loop {file} {mountpoint}")
		data = command_line(f"sudo tar c --one-file-system --directory={mountpoint} .")
		h = hashlib.sha1()
		h.update(data)
		del data
		ret = h.hexdigest()
		logger.debug("Digest: %s", ret)
		return ret
	except subprocess.SubprocessError as e:
		logger.debug("Failed: %s", e)
		return
	finally:
		command_line(f"sudo umount {mountpoint} ; true")


def fuzz_mount_report(src_file: str, dst_file: str, mountpoint: str, zzuf_options: str=""):

	src_checksum = mount_checksum(src_file, mountpoint)
	assert src_checksum is not None

	for rnd_seed in itertools.count():
		command_line(f"zzuf {zzuf_options} -s {rnd_seed} < {src_file} > {dst_file}")

		dst_checksum = mount_checksum(dst_file, mountpoint)
		if dst_checksum is None:
			logger.info("Fuzzed file couldn't mount or fully read.")
		elif src_checksum != dst_checksum:
			logger.warning("Checksum of fuzzed and original files differ. \n - Original: %s\n - Fuzzed:   %s" %(src_checksum, dst_checksum))
			break
		else:
			logger.info("Checksum of fuzzed and original files are identical: %s" %src_checksum)

if __name__ == '__main__':

	import argparse

	parser = argparse.ArgumentParser(
	 description="Fuzz a filesystem, try to mount it, and compare checksums",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	parser.add_argument("src_file",
	 help="Filesystem to be fuzzed",
	)

	parser.add_argument("dst_file",
	 help="Fuzzed filesystem name",
	)

	parser.add_argument("mountpoint",
	 help="Mountpoint directory",
	)

	parser.add_argument("--zzuf-options",
	 help="Fuzz options. Fefer to caca.zoy.org/wiki/zzuf",
	 default='',
	)

	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args()

	logging.basicConfig(
	 level=getattr(logging, args.log_level),
	 format="%(levelname)s %(message)s"
	)

	fuzz_mount_report(args.src_file, args.dst_file, args.mountpoint, args.zzuf_options)