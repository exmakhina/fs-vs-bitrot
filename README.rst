############
Bitrot Tests
############

Filesystem Tests
################


When a block device hosting a filesystem is subject to bit rot:

- the filesystem can be self-healing or not;
- the filesystem can detect the bit rot or not;
