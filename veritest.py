#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Test dm-verity ... recreate dm device during each test

import sys, io, os, re, subprocess, logging, argparse, itertools, hashlib, contextlib, time

logger = logging.getLogger()

class VerityBench:
	verity_datad = "/tmp/verity-ref"
	verity_hashd = "/tmp/verity-b"
	verity_fecd = "/tmp/verity-c"
	fec_roots = 3
	dm_name = "test-verity"

	def prepare(self, size=300<<20):
		if os.path.exists(self.verity_datad):
			os.unlink(self.verity_datad)

		if 1:
			cmd = [
			 "truncate",
			  f"--size={size}",
			  self.verity_datad,
			]
		else:
			cmd = [
			 "dd", "if=/dev/zero", "of={}".format(self.verity_datad),
			 "bs={}".format(size), "count=1",
			]
		logger.debug("Running %s", cmd)
		subprocess.run(cmd, check=True)
		cmd = [
		 "veritysetup",
		  "format",
		   self.verity_datad,
		   self.verity_hashd,
		   "--fec-device", self.verity_fecd,
		   "--fec-roots={}".format(self.fec_roots),
		   "--salt=00",
		   "--uuid={}".format("c5a432c1-f5e4-457e-ac79-3c89da261f3d"),
		]
		logger.debug("Running %s", cmd)
		res = subprocess.run(cmd,
		 check=True,
		 stdout=subprocess.PIPE,
		)
		stdout = res.stdout.decode("utf-8")
		for line in stdout.splitlines():
			logger.info("> %s", line)
		self.verity_roothash = stdout.splitlines()[-1].split()[-1].rstrip()
		logger.info("Root hash: %s", self.verity_roothash)
		cmd = [
		 "sha1sum",
		  "--binary",
		  self.verity_datad,
		  self.verity_hashd,
		  self.verity_fecd,
		]
		logger.debug("Running %s", cmd)
		res = subprocess.run(cmd,
		 check=True,
		 stdout=subprocess.PIPE,
		)
		stdout = res.stdout.decode("utf-8")
		for line in stdout.splitlines():
			logger.info("> %s", line)

	def verify(self, errors):
		cmd = [
		 "veritysetup",
		 "verify",
		  self.verity_datad,
		  self.verity_hashd,
		  self.verity_roothash,
		  "--fec-device", self.verity_fecd,
		  "--fec-roots={}".format(self.fec_roots),
		]
		logger.debug("Running %s", cmd)
		res = subprocess.run(cmd,
		 stdout=subprocess.PIPE,
		 stderr=subprocess.PIPE
		)
		logger.info("Return: %s", res.returncode)
		stdout = res.stdout.decode("utf-8")
		for line in stdout.splitlines():
			logger.info("O %s", line)
		stderr = res.stderr.decode("utf-8")
		for line in stderr.splitlines():
			logger.info("E %s", line)

		last = stderr.splitlines()[-1].strip()
		m = re.match("^Found (?P<count>\S+) repairable errors with FEC device.$", last)
		if m is not None:
			corrected = int(m.group("count"))
			if corrected < errors:
				raise subprocess.CalledProcessError(res.returncode, cmd)

		if "cannot" in stderr:
			raise subprocess.CalledProcessError(res.returncode, cmd)

	@contextlib.contextmanager
	def create(self):
		cmd = [
		 "veritysetup",
		 "create",
		  self.dm_name,
		  self.verity_datad,
		  self.verity_hashd,
		  self.verity_roothash,
		  "--fec-device", self.verity_fecd,
		  "--fec-roots={}".format(self.fec_roots),
		]
		cmd = ["sudo"] + cmd
		logger.debug("Running %s", cmd)
		subprocess.run(cmd,
		 check=True,
		)

		dm_path = "/dev/mapper/" + self.dm_name

		cmd = [
		 "chown",
		  "{}".format(os.getuid()),
		  dm_path,
		]
		cmd = ["sudo"] + cmd
		logger.debug("Running %s", cmd)
		subprocess.run(cmd,
		 check=True,
		)

		while False:
			try:
				cmd = f"dd if={dm_path} of=/dev/null status=none"
				logger.debug("Running %s", cmd)
				subprocess.run(cmd,
				 check=True,
				 shell=True,
				)
				break
			except Exception as e:
				time.sleep(1)
				continue

		try:
			yield dm_path
		finally:
			dt = 1
			for i in range(90):
				cmd = [
				 "udevadm",
				 "settle"
				]
				cmd = ["sudo"] + cmd
				logger.debug("Running %s", cmd)
				res = subprocess.run(cmd)
				cmd = [
				 "dmsetup",
				  "remove",
				  "--retry", self.dm_name,
				]
				cmd = ["sudo"] + cmd
				logger.debug("Running %s", cmd)
				res = subprocess.run(cmd)
				if res.returncode == 0:
					break
				time.sleep(dt)
				dt *= 1.5
			else:
				logger.warning("dmsetup remove yourself")
				raise RuntimeError()

if __name__ == '__main__':

	import argparse

	parser = argparse.ArgumentParser(
	 description="Assess dm-verity performance",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	parser.add_argument("--check-with",
	 choices=("kernel", "veritysetup"),
	 default="kernel",
	)

	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args()

	logging.basicConfig(
	 level=getattr(logging, args.log_level),
	 format="%(levelname)s %(message)s"
	)

	bench = VerityBench()
	bench.prepare()

	verity_datad_ref = bench.verity_datad

	span_max = 10 << 20
	span_min = 0
	span_l = span_min
	span_r = span_max

	for idx_attempt in itertools.count():
		logger.info("- %d: %s-%s", idx_attempt, span_l, span_r)
		if span_r - span_l <= 1:
			break

		verity_datad_mes = "/tmp/verity-fuzz"
		bench.verity_datad = verity_datad_mes

		cmd = [
		 "cp", verity_datad_ref, bench.verity_datad,
		]
		logger.debug("Running %s", cmd)
		subprocess.run(cmd,
		 check=True,
		)

		span_c = (span_l + span_r) // 2

		logger.info("  Fuzzing %d bytes at the beginning", span_c)
		span_buf = (b"!" * span_c)
		fd = os.open(bench.verity_datad, os.O_RDWR)
		x = os.write(fd, span_buf)
		assert x == len(span_buf)
		os.fdatasync(fd)
		os.close(fd)

		try:
			if args.check_with == "veritysetup":
				bench.verify(span_c)
			else:
				with bench.create() as wrapped:
					cmd = f"cmp {wrapped} {verity_datad_ref}"
					logger.debug("Running %s", cmd)
					subprocess.run(cmd,
					 check=True,
					 shell=True,
					)
			logger.info("  Pass")
			span_l = span_c
		except subprocess.CalledProcessError as e:
			logger.info("  Fail")
			span_r = span_c

